require_relative 'parser'
puts "Starting parsing logs\n"
logs = Parser.parse
# logs.each {|log| puts log}
puts "Logs successfully parsed.\n"
puts "Choose filter option. The logs will be written to file \"output.log\""
puts "\n1.By IP address.\n2. By method.\n3. By local address.\n4.Last 50."
print '(1, 2, 3, 4): '
choice = gets.chomp.to_i
output = []
if choice == 1
  print "\nEnter IP address: "
  choice = gets.chomp
  output = logs.select {|log| log.user_ip == choice}
elsif choice == 2
  print "\nEnter METHOD address: "
  choice = gets.chomp
  output = logs.select {|log| log.method == choice}

elsif choice == 3
  print "\nEnter LOCAL address: "
  choice = gets.chomp
  output = logs.select {|log| log.address == choice}
elsif choice == 4
  output = logs[(logs.size - 50)..(logs.size - 1)]

else
  puts "\nInvalid input."


end

lines_to_write = []
output.each {|o| lines_to_write << o.raw_log}
file = File.open('output.log', 'w+')
lines_to_write.each {|line| file.write line}
file.close

# ip_adress = '95.134.63.162'
# fedor = logs.select {|log| log.user_ip == ip_adress}
# fedor.each {|f| puts f.raw_log}