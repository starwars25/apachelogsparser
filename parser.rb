require_relative 'log_element'

class Parser
  def self.parse
    file = File.open '/var/logs/apache2/access.log', 'r'
    data = file.readlines
    file.close
    parse_strings data
  end

  private
  def self.parse_strings(array)
    output = []
    array.each do |string|
      begin
        arr = string.split "\""
        user_agent = arr[5]
        response_code = arr[2].split(' ')[0]
        ip = arr[0].split(' ')[0]
        method = arr[1].split(' ')[0]
        address = arr[1].split(' ')[1]
        date = arr[0].split('[')[1].split(']')[0]
        output << LogElement.new(user_agent, date, ip, address, response_code, method, string)
          # puts arr
      rescue
        # ignored
      end
    end
    output
  end
end