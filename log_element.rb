class LogElement
  attr_reader :method, :address, :response_code, :user_agent, :date, :user_ip, :raw_log

  def initialize(ua, d, ui, a, r, m, log)
    @user_agent = ua
    @date = d
    @user_ip = ui
    @address = a
    @response_code = r
    @method = m
    @raw_log = log
  end

  @user_agent
  @date
  @user_ip
  @address
  @response_code
  @method
  @raw_log

  def to_hash
    {'user_agent' => @user_agent, 'date' => @date, 'user_ip' => @user_ip, 'address' => @address,
    'response_code' => @response_code, 'method' => @method}
  end

  def to_s
    "#{@user_ip} #{@date} #{@user_agent} #{@method} #{@address} #{@response_code}"
  end

  def self.print_array(array)
    file = File.open('output.log', 'w+')
    array.each {|line| file.write line.raw_log}
    file.close
  end

end